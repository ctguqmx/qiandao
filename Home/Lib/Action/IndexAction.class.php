<?php
// 本类由系统自动生成，仅供测试用途
class IndexAction extends Action {
    public function index(){
		//接受从论坛传来的值
		$uid=$_GET['var'];
		//$time=time();
		//echo $time;
		$_SESSION['id']=$uid;
		$user=M('User');
		$data['uid']=$uid;
		$res=$user->where($data)->getField('username');
		if($res!=''){
			$_SESSION['name']=$res;
			$this->redirect('__APP__/Qiandao/index');
		}
		else{
			$this->redirect('init');
		}
	}

	public function init(){
		//初始化
		$group=M('Group');
		$groarr=$group->select();
		$this->assign('groarr',$groarr);
		$this->display();
	}

	public function doInit(){
		//实现初始化
		$group=$_POST['group'];
		$username=$_POST['username'];
		if($username==''){
			$this->error('用户名不能为空','init');
		}
		$user=M('User');
		$id=$_SESSION['id'];
		$data[uid]=$id;
		$data[username]=$username;
		$data[group]=$group;
		$_SESSION['name']=$username;
		$user->data($data)->add();
		if($user==1){
			$this->success('账号初始化成功',U('Qiandao/index'));
		}else{
			$this->error('初始化账号错误');
		}
	}
}
