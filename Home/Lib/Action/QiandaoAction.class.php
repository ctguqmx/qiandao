<?php
	class QiandaoAction extends CommonAction{
		public function index(){
			//显示签到按钮，今日签到情况
			//获取今天周几
			$xingqi=date('w');
			$rec=M('Rec');
			$data[uid]=$_SESSION['id'];
			//读取周次
			$qdtime=M('qdtime');
			$week=$qdtime->where('id=1')->getField('week');
			$data['week']=$week;

			$res=$rec->where($data)->find();
			$this->assign('vo',$res);
			$this->assign('xq',$xingqi);

			//获取允许签到的时间
			$qdtime=M('Qdtime');
			$qtlist=$qdtime->select();
			$this->assign('qtlist',$qtlist[0]);
			$this->display();
		}

		public function showinfo(){
			//显示该组签到情况
			$groupId=$_POST['groid'];
			$selweek=$_POST['selweek'];
			if($groupId==''){
				//获取该用户组别
				$user=M('User');
				$data['uid']=$_SESSION['id'];
				$groupId=$user->where($data)->getField('group');
			}

			//获取组别给前台的select
			$group=M('Group');
			$groarr=$group->select();
			$this->assign('groarr',$groarr);

			//查找该组的所有用户
			$user=M('User');
			$duser['group']=$groupId;
			$ulist=$user->where($duser)->select();
			
			//获取周次
			$qdtime=M('Qdtime');
			$week=$qdtime->where('id=1')->getField('week');
			if($selweek!=''){
				$data['week']=$selweek;
			}
			else{
				$data['week']=$week;
			}
				
			//遍历所有该组用户本周的签到信息
			foreach($ulist as $key){
				$rec=M('Rec');
				$data[uid]=$key[uid];
				$res=$rec->where($data)->select();
				$qdlist[]=$res[0];
			}
			
			//sum排序
			$qdlist=array_sort($qdlist,'sum');

			$this->assign('weeknum',$week);
			$this->assign('qdlist',$qdlist);
			$this->assign('groupId',$groupId);
			$this->assign('selweek',$data[week]);
			$this->display();
		}
		public function doQd(){
			import('ORG.Util.Date');// 导入日期类
			//实现签到
			//判断的项目
			//ip，是否在签到时间内
			//设置的项目：星期，周次，用户

			//读出ip
			$clip = get_client_ip();
			//$clip = $_SERVER["REMOTE_ADDR"];
			//echo $clip;
			$ip=M('ip');
			$dbip=$ip->find();
			$dbip=$dbip[ip];
			$dbip=explode('.',$dbip);
			$iparr=explode('.',$clip);
			if($iparr[0]==$dbip[0] && $iparr[1]==$dbip[1] && $iparr[2]==$dbip[2]){
			//if(1){
				//获取允许签到的时间
				$qdtime=M('Qdtime');
				$timearr=$qdtime->where('id=1')->find();
				
				//获取当前时间的小时和分钟
				$nowtime=time();
				$hour=date('G',$nowtime);
				$min=date('i',$nowtime);
				
				//判断时间是否在签到时间内
				$flag=0;
				if($hour==$timearr['3eh'] && $min<=$timearr['3em']){
						$flag=4;
				}
				else if($hour>$timearr['3sh'] && $hour<$timearr['3eh']){
					$flag=4;
				}
				else if($hour==$timearr['3sh'] && $min>=$timearr['3sm']){
					$flag=4;
				}

				if($hour==$timearr['2eh'] && $min<=$timearr['2em']){
						$flag=2;
				}
				else if($hour>$timearr['2sh'] && $hour<$timearr['2eh']){
					$flag=2;
				}
				else if($hour==$timearr['2sh'] && $min>=$timearr['2sm']){
					$flag=2;
				}

				if($hour==$timearr['1eh'] && $min<=$timearr['1em']){
						$flag=1;
				}
				else if($hour>$timearr['1sh'] && $hour<$timearr['1eh']){
					$flag=1;
				}
				else if($hour==$timearr['1sh'] && $min>=$timearr['1sm']){
					$flag=1;
				}

				//0不在签到时间内
				if($flag==0){
					$this->error('请在规定时间签到');
				}
				else{
					//从数据库读出数据，并判断写入
					$rec=M('Rec');
					$data['uid']=$_SESSION['id'];
					//判断今天的日期
					//$date=date('Y-m-d');
					//$data['qddate']=$date;
					
					//获取今天周几
					$xingqi=date('w');
					//$xingqi=1;
					//读取周次,本周周一时间，第一周周一时间
					$qdtime=M('qdtime');
					$week=$qdtime->where('id=1')->getField('week');
					$monday=$qdtime->where('id=1')->getField('monday');
					$firstweekmonday=$qdtime->where('id=1')->getField('firstweekmonday');
					//更新周次
					if($xingqi==1){
						//获取今天的时间
						$today=date("Y-m-d",time());
						//时间类
						//$today='2014-3-11';
						$Date=new Date($today);
						//今天和数据库中本周第一天时间差
						$daydiff= $Date->dateDiff($monday,'d');
						if($daydiff<=-7){
							//更新周次
							$today_wf=$Date->dateDiff($firstweekmonday);
							$dqdtime['week']=(-$today_wf)/7+1;
							//更新本周周一
							$today_monday=$today_wf%7;//今天距离本周周一的天数
							$thismonday=$Date->dateAdd($today_monday);
							//格式化date类
							$dqdtime[monday]=$thismonday->format("%Y-%m-%d");
							$qdtime->where('id=1')->save($dqdtime);
						}
					}
					//获取周次
					$data['week']=$qdtime->where('id=1')->getField('week');
					//查询数据是否存在,即本周是否签过到，并过去本周签到记录id
					$gid=$rec->where($data)->getField('id');
					if($gid==''){
						//$data[$xingqi]=$flag;
						//$data['lastqd']=$flag;
						$data['name']=$_SESSION['name'];
						$gid1=$rec->add($data);
						$data['id']=$gid1;
					}
					else{
						$data['id']=$gid;
					}
					//}
					//else{
						//获取本周数据id
						//$gid=$rec->where($data)->getField('id');
						//$data['id']=$gid;
					//}

					//更新签到情况
					//查看今天是否已经签到
					$todayQd=$rec->where($data)->find();
					//$todayQd[$xingqi]今天的签到情况
					if($todayQd[$xingqi]==''){
						$data[$xingqi]=$flag;
						$data['lastqd']=$flag;
						$data['sum']=$todayQd['sum']+1;
						//更新数据库
						$rec->save($data);
						$this->success('签到成功');
					}
					else if($todayQd[lastqd]==$flag){
						$this->error('今天你已经签过到');
					}
					else{
						$data[$xingqi]=$todayQd[$xingqi]+$flag;
						$data['lastqd']=$flag;
						$data['sum']=$todayQd['sum']+1;
						//更新数据库
						//dump($data);
						$rec->save($data);
						$this->success('签到成功');
					}
				}
			}
			else{
					$this->error('请在工作室内签到');
			}
		}
	}
?>
