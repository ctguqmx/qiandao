/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : qmxqd

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2014-03-13 17:46:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `qd_group`
-- ----------------------------
DROP TABLE IF EXISTS `qd_group`;
CREATE TABLE `qd_group` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `groupname` char(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qd_group
-- ----------------------------
INSERT INTO `qd_group` VALUES ('1', '硬件组');
INSERT INTO `qd_group` VALUES ('2', '软件组');
INSERT INTO `qd_group` VALUES ('3', 'WEB组');
INSERT INTO `qd_group` VALUES ('4', '数媒组');
INSERT INTO `qd_group` VALUES ('5', '新生培训');

-- ----------------------------
-- Table structure for `qd_ip`
-- ----------------------------
DROP TABLE IF EXISTS `qd_ip`;
CREATE TABLE `qd_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qd_ip
-- ----------------------------
INSERT INTO `qd_ip` VALUES ('1', '192.168.242.0');

-- ----------------------------
-- Table structure for `qd_qdtime`
-- ----------------------------
DROP TABLE IF EXISTS `qd_qdtime`;
CREATE TABLE `qd_qdtime` (
  `id` int(1) NOT NULL DEFAULT '0',
  `1sh` int(11) DEFAULT NULL,
  `1eh` int(11) DEFAULT NULL,
  `2sh` int(11) DEFAULT NULL,
  `2eh` int(11) DEFAULT NULL,
  `3sh` int(11) DEFAULT NULL,
  `3eh` int(11) DEFAULT NULL,
  `1sm` int(11) DEFAULT NULL,
  `1em` int(11) DEFAULT NULL,
  `2sm` int(11) DEFAULT NULL,
  `2em` int(11) DEFAULT NULL,
  `3sm` int(11) DEFAULT NULL,
  `3em` int(11) DEFAULT NULL,
  `week` int(2) DEFAULT NULL,
  `monday` date NOT NULL,
  `firstweekmonday` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qd_qdtime
-- ----------------------------
INSERT INTO `qd_qdtime` VALUES ('1', '9', '12', '12', '15', '15', '22', '10', '30', '35', '10', '15', '10', '4', '2014-03-10', '2014-02-17');

-- ----------------------------
-- Table structure for `qd_rec`
-- ----------------------------
DROP TABLE IF EXISTS `qd_rec`;
CREATE TABLE `qd_rec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `uid` int(5) NOT NULL,
  `1` int(1) DEFAULT NULL,
  `2` int(1) DEFAULT NULL,
  `3` int(1) DEFAULT NULL,
  `4` int(1) DEFAULT NULL,
  `5` int(1) DEFAULT NULL,
  `6` int(1) DEFAULT NULL,
  `0` int(1) DEFAULT NULL,
  `sum` int(2) NOT NULL DEFAULT '0',
  `week` int(2) NOT NULL DEFAULT '0',
  `lastqd` int(11) DEFAULT NULL,
  `qddate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qd_rec
-- ----------------------------
INSERT INTO `qd_rec` VALUES ('1', '1111', '1', '4', null, null, null, null, null, null, '1', '2', '4', null);
INSERT INTO `qd_rec` VALUES ('4', '44', '4', '4', null, null, null, null, null, null, '4', '4', '4', null);
INSERT INTO `qd_rec` VALUES ('6', '7', '7', '4', null, null, null, null, null, null, '3', '4', '4', null);
INSERT INTO `qd_rec` VALUES ('7', '66', '6', null, '2', null, null, null, null, null, '4', '4', '2', null);
INSERT INTO `qd_rec` VALUES ('9', '1111', '1', '4', null, '4', null, null, null, null, '2', '4', '4', null);

-- ----------------------------
-- Table structure for `qd_user`
-- ----------------------------
DROP TABLE IF EXISTS `qd_user`;
CREATE TABLE `qd_user` (
  `uid` int(5) NOT NULL,
  `username` varchar(20) NOT NULL,
  `group` int(1) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qd_user
-- ----------------------------
INSERT INTO `qd_user` VALUES ('1', '1111', '5');
INSERT INTO `qd_user` VALUES ('4', '44', '5');
INSERT INTO `qd_user` VALUES ('6', '66', '5');
INSERT INTO `qd_user` VALUES ('7', '7', '5');
INSERT INTO `qd_user` VALUES ('81', '李娅', '5');
