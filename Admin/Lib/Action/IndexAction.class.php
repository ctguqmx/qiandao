<?php
// 本类由系统自动生成，仅供测试用途
class IndexAction extends Action {
    public function index(){
		//显示登录
    	$this->display();
	}

	public function doLogin(){
		//验证登录
		$username=$_POST['username'];
		$password=$_POST['password'];
		$password=md5($password);
		if($username=='admin' && $password=='21232f297a57a5a743894a0e4a801fc3'){
			$this->success('登录成功','showAdmin');
		}
		else{
			$this->error('登录失败');
		}
	}

	public function showAdmin(){
		//显示框架
		$this->display();
	}
	
	public function left(){
		//框架左侧栏目
		$this->display();
	}

	public function yhgl(){
		//用户管理
		$user=M('User');
		$list=$user->select();
		$this->assign('list',$list);
		$this->display();
	}
	
	public function sjgl(){
		//时间管理
		$qdtime=M('Qdtime');
		$list=$qdtime->select();
		$this->assign('list',$list[0]);
		$this->display();
	}


	public function setAllowTime(){
		//设置允许签到的时间
		$this->display();
	}

	public function doSetTime(){
		//实现签到时间更新
		$data['1s']=$_POST['1s'];
		$data['1e']=$_POST['1e'];
		$data['2s']=$_POST['2s'];
		$data['2e']=$_POST['2e'];
		$data['3s']=$_POST['3s'];
		$data['3e']=$_POST['3e'];
		$qdtime=M('Qdtime');
		$res=$qdtime->data($data)->add();
	}
	

	public function del(){
		//删除用户
		$uid=$_GET['uid'];
		$user=M('User');
		$data['uid']=$uid;
		if($user->where($data)->delete()){
			$this->redirect('Index/yhgl');
		}
		else{
			$this->error('删除失败');
		}
	}

	public function doChangeTime(){
		//修改允许签到的时间
		$data['id']=1;

		if($_POST['t1sh']!=''){
			$data['1sh']=$_POST['t1sh'];
		}
		if($_POST['t1sm']!=''){
			$data['1sm']=$_POST['t1sm'];
		}
		if($_POST['t2sh']!=''){
			$data['2sh']=$_POST['t2sh'];
		}
		if($_POST['t2sm']!=''){
			$data['2sm']=$_POST['t2sm'];
		}
		if($_POST['t3sh']!=''){
			$data['3sh']=$_POST['t3sh'];
		}
		if($_POST['t3sm']!=''){
			$data['3sm']=$_POST['t3sm'];
		}
		
		if($_POST['t1eh']!=''){
			$data['1eh']=$_POST['t1eh'];
		}
		if($_POST['t1em']!=''){
			$data['1em']=$_POST['t1em'];
		}
		if($_POST['t2eh']!=''){
			$data['2eh']=$_POST['t2eh'];
		}
		if($_POST['t2em']!=''){
			$data['2em']=$_POST['t2em'];
		}
		if($_POST['t3eh']!=''){
			$data['3eh']=$_POST['t3eh'];
		}
		if($_POST['t3em']!=''){
			$data['3em']=$_POST['t3em'];
		}
		$qdtime=M('Qdtime');
		$res=$qdtime->save($data);
		if($res!=''){
			$this->redirect('Index/sjgl');
		}
		else{
			$this->error('修改失败');
		}
	}

	public function ipgl(){
		//ip管理
		$qdip=M('Ip');
		$arr=$qdip->select();
		$this->assign('list',$arr);
		$this->display();
	}

}

